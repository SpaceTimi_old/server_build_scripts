#!/bin/bash

if [ $# -ne 2 ]; then
    printf "Usage!! pull_app.sh <app name> <path to server root>\n";
    exit
fi

invocation_dir=$PWD;

app_name=$1
if [ $app_name != "bonda" ]; then
    printf "$app_name is not an app\n";
    exit
fi

server_root_dir=$2
app_dir=$server_root_dir"/"$app_name"_server";
patch_files_dir=$server_root_dir"/server_build_scripts/patch_files/"$app_name;

# Switch to repo dir
cd $app_dir;
printf "\n********************************************************************************\n";
printf "Pulling code into $app_name\n\n";
git reset HEAD --hard --quiet;
git pull;
printf "\nHead is now at:\n▶ ";
git log --oneline -5 | head
# Switch back to invocation dir so that relative paths still work for subsequent commands
cd $invocation_dir;

printf "\n********************************************************************************\n";

# Copy any app specific patch files:
if [ $app_name == "bonda" ]; then
    # Patch AppConfig.json
    destination_file=$app_dir"/Config/AppConfig.json";
    source_file=$patch_files_dir"/AppConfig.json";
    printf "Patching AppConfig.json\n";

    if [ -e $source_file ]; then
        cp -r $source_file $destination_file;
    else
        printf "Failed to patch AppConfig for "$app_name"\n";
    fi
fi

