#!/bin/bash

if [ $# -ne 1 ]; then
    printf "Usage!! pull_shared.sh <path to server root>\n";
    exit
fi

invocation_dir=$PWD;
server_root_dir=$1
timi_shared_dir=$server_root_dir"/timi_shared_server";
patch_files_dir=$server_root_dir"/server_build_scripts/patch_files/shared";

cd $timi_shared_dir;

printf "\n********************************************************************************\n";
printf "Pulling code into timi_shared_server\n\n";

git reset HEAD --hard --quiet;
git pull;
printf "\nHead is now at:\n▶ ";
git log --oneline -5 | head

# Switch back to invocation dir so that relative paths still work for subsequent commands
cd $invocation_dir;

printf "\n********************************************************************************\n";

# Copy shared patch files:
    # Patch SharedConfig.json
    destination_file=$timi_shared_dir"/src/Config/SharedConfig.json";
    source_file=$patch_files_dir"/SharedConfig.json";
    printf "Patching SharedConfig.json\n";

    if [ -e $source_file ]; then
        cp -r $source_file $destination_file;
    else
        printf "Failed to patch SharedConfig\n";
    fi

